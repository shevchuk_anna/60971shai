<?php
namespace App\Controllers;

use CodeIgniter\Controller;
use IonAuth\Libraries\IonAuth;
use App\Services\GoogleClient;
use App\Services\IonAuthGoogle;

class BaseController extends Controller
{

    protected $helpers = [];
    protected $ionAuth;
    protected $google_client;
    /**
     * Constructor.
     */
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);

        $this->google_client = new GoogleClient();
        $this->ionAuth = new IonAuth();
    }
    protected function withIon(array $data = [])
    {
        $data['ionAuth'] = $this->ionAuth;
        $data['authUrl'] = $this->google_client->getGoogleClient()->createAuthUrl();
        return $data;
    }
}