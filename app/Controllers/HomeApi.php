<?php

namespace App\Controllers;

use App\Models\OAuthModel;
use CodeIgniter\RESTful\ResourceController;
use App\Services\OAuth;
use OAuth2\Request;

class HomeApi extends ResourceController
{
    protected $modelName = 'App\Models\HotelModel';
    protected $format = 'json';
    protected $oauth;
    public function __construct()
    {
        Header('Access-Control-Allow-Origin: *'); //for allow any domain, insecure
        Header('Access-Control-Allow-Headers: *'); //for allow any headers, insecure
        Header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE'); //method
    }
    public function delete($id = null)
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isloggedIn()) {

            $model = $this->model;
            $model->delete($id);
            return $this->respondDeleted(null, 'Room deleted successfully');
        }

        $this->oauth->server->getResponse()->send();
    }
    public function store()
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;
            if ($this->request->getMethod() === 'post' && $this->validate([
                    'ID_HOUSING' => 'required',
                    'NumRoom' => 'required',
                    'NumOfSeats' => 'required',
                    'Cost' => 'required',
                ])) {
                $query = $model->db->table('users')
                    ->select('*')
                    ->where('username', $OAuthModel->getUser()->user_id)
                    ->limit(1)
                    ->get();
                $id = ($query->getRow())->id;
                //подготовка данных для модели
                $data = [
                    'ID_HOUSING' => $this->request->getPost('ID_HOUSING'),
                    'NumRoom' => $this->request->getPost('NumRoom'),
                    'NumOfSeats' => $this->request->getPost('NumOfSeats'),
                    'Cost' => $this->request->getPost('Cost'),
                    'user_id' => $id,
                    'picture' => ''
                ];
                $model->save($data);
                return $this->respondCreated(null, 'Room created successfully');
            } else {
                return $this->respond($this->validator->getErrors());
            }
        } else $this->oauth->server->getResponse()->send();
    }

    public function room() //Отображение всех записей
    {
        $this->oauth = new OAuth();
        if ($this->oauth->isLoggedIn()) {
            $OAuthModel = new OAuthModel();
            $model = $this->model;
            $per_page = $this->request->getPost('per_page');
            $search = $this->request->getPost('search');
            $query = $model->db->table('users')
                ->select('*')
                ->where('username', $OAuthModel->getUser()->user_id)
                ->limit(1)
                ->get();
            $id = ($query->getRow())->id;
            $data = $this->model->getRooms($id, $search, $per_page);
            //Ответ контроллера включает данные (ratings) и параметры пагинации (pager)
            return $this->respond(['Комната' => $data, 'pager' => $model->pager->getDetails('group1')]);
        } else $this->oauth->server->getResponse()->send();
    }

}