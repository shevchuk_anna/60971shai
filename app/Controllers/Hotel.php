<?php
namespace App\Controllers;

use CodeIgniter\Model;
use App\Models\HotelModel;
use Aws\S3\S3Client;

class Hotel extends BaseController
{
    public function index() //Обображение всех записей
    {
        if (!$this->ionAuth->loggedIn())
        {
            return redirect()->to('/auth/login');
        }
        $model = new HotelModel();
        $data ['room'] = $model->getRoom();
        echo view('room/view_all', $this->withIon($data));
    }

    public function view($id = null) //отображение одной записи
    {
        //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new HotelModel();
        $data ['room'] = $model->getRoom($id);
        echo view('room/view', $this->withIon($data));
    }

    public function store()
    {
        helper(['form', 'url']);

        if ($this->request->getMethod() === 'post' && $this->validate([
                'NumRoom' => 'required|numeric',
                'NumOfSeats' => 'required|numeric',
                'Cost' => 'required',
                'picture' => 'is_image[picture]|max_size[picture,1024]',
            ])) {
            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }
            $model = new HotelModel();
            $data = [
                'ID_HOUSING' => 4,
                'NumRoom' => $this->request->getPost('NumRoom'),
                'NumOfSeats' => $this->request->getPost('NumOfSeats'),
                'Cost' => $this->request->getPost('Cost'),
                'user_id' => $this->ionAuth->user()->row()->id,
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Комната создана успешно!'));
            return redirect()->to('/hotel');
        }
        else {
            return redirect()->to('/hotel/create')->withInput();
        }
    }

    public function create()
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        helper(['form']);
        $data ['validation'] = \Config\Services::validation();
        echo view('room/create', $this->withIon($data));
    }

    public function edit($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new HotelModel();

        helper(['form']);
        $data ['room'] = $model->getRoom($id);
        $data ['validation'] = \Config\Services::validation();
        echo view('room/edit', $this->withIon($data));
    }

    public function update()
    {
        helper(['form', 'url']);

        echo '/hotel/edit/' . $this->request->getPost('id');

        if ($this->request->getMethod() === 'post' && $this->validate([
                'id' => 'required',
                'NumRoom' => 'required',
                'NumOfSeats' => 'required',
                'Cost' => 'required',
                'picture' => 'is_image[picture]|max_size[picture,1024]',
            ])) {
            $insert = null;
            $file = $this->request->getFile('picture');
            if ($file->getSize() != 0) {
                //подключение хранилища
                $s3 = new S3Client([
                    'version' => 'latest',
                    'region' => 'us-east-1',
                    'endpoint' => getenv('S3_ENDPOINT'),
                    'use_path_style_endpoint' => true,
                    'credentials' => [
                        'key' => getenv('S3_KEY'), //чтение настроек окружения из файла .env
                        'secret' => getenv('S3_SECRET'), //чтение настроек окружения из файла .env
                    ],
                ]);
                //получение расширения имени загруженного файла
                $ext = explode('.', $file->getName());
                $ext = $ext[count($ext) - 1];
                //загрузка файла в хранилище
                $insert = $s3->putObject([
                    'Bucket' => getenv('S3_BUCKET'), //чтение настроек окружения из файла .env
                    //генерация случайного имени файла
                    'Key' => getenv('S3_KEY') . '/file' . rand(100000, 999999) . '.' . $ext,
                    'Body' => fopen($file->getRealPath(), 'r+')
                ]);
            }
            $model = new HotelModel();
            $data = [
                'id' => $this->request->getPost('id'),
                'NumRoom' => $this->request->getPost('NumRoom'),
                'NumOfSeats' => $this->request->getPost('NumOfSeats'),
                'Cost' => $this->request->getPost('Cost'),
                'user_id' => $this->ionAuth->user()->row()->id,
            ];
            if (!is_null($insert))
                $data['picture_url'] = $insert['ObjectURL'];
            $model->save($data);
            session()->setFlashdata('message', lang('Обновление прошло успешно!'));
            return redirect()->to('/hotel');
        } else {
            return redirect()->to('/hotel/edit/' . $this->request->getPost('id'))->withInput();
        }
    }

    public function delete($id)
    {
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        $model = new HotelModel();

        $model->delete($id);
        session()->setFlashdata('message', lang('Удаление прошло успешно!'));
        return redirect()->to('/hotel/viewAllWithUsers');

    }

    public function viewAllWithUsers()
    {
        if ($this->ionAuth->isAdmin())
        {
            //Подготовка значения количества элементов выводимых на одной странице
            if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
            {
                //сохранение кол-ва страниц в переменной сессии
                session()->setFlashdata('per_page', $this->request->getPost('per_page'));
                $per_page = $this->request->getPost('per_page');
            }
            else {
                $per_page = session()->getFlashdata('per_page');
                session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
                if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
            }
            $data['per_page'] = $per_page;

            //Обработка запроса на поиск
            if (!is_null($this->request->getPost('search')))
            {
                session()->setFlashdata('search', $this->request->getPost('search'));
                $search = $this->request->getPost('search');
            }
            else {
                $search = session()->getFlashdata('search');
                session()->setFlashdata('search', $search);
                if (is_null($search)) $search = '';
            }
            $data['search'] = $search;

            helper(['form','url']);

            $model = new HotelModel();
            $data['room'] = $model->getRoomWithUser(null,$search)->paginate($per_page, 'group1');
            $data['pager'] = $model->pager;
            echo view('room/view_all_with_users', $this->withIon($data));
        }
        else
        {
            session()->setFlashdata('message', lang('Доступ только для администратора'));
            return redirect()->to('/auth/login');
        }
    }
}
/*
         //если пользователь не аутентифицирован - перенаправление на страницу входа
        if (!$this->ionAuth->loggedIn()) {
            return redirect()->to('/auth/login');
        }
        //Подготовка значения количества элементов выводимых на одной странице
        if (!is_null($this->request->getPost('per_page'))) //если кол-во на странице есть в запросе
        {
            //сохранение кол-ва страниц в переменной сессии
            session()->setFlashdata('per_page', $this->request->getPost('per_page'));
            $per_page = $this->request->getPost('per_page');
        }
        else {
            $per_page = session()->getFlashdata('per_page');
            session()->setFlashdata('per_page', $per_page); //пересохранение в сессии
            if (is_null($per_page)) $per_page = '5'; //кол-во на странице по умолчанию
        }
        $data['per_page'] = $per_page;
        //Обработка запроса на поиск
        if (!is_null($this->request->getPost('search')))
        {
            session()->setFlashdata('search', $this->request->getPost('search'));
            $search = $this->request->getPost('search');
        }
        else {
            $search = session()->getFlashdata('search');
            session()->setFlashdata('search', $search);
            if (is_null($search)) $search = '';
        }
        $data['search'] = $search;
        helper(['form','url']);

        $model = new HotelModel();
        $data ['room'] = $model->getRoom();
        $data['room'] = $model->getRoomWithUser(null, $search)->paginate($per_page, 'group1');
        if(!count($data['room'])){
            session()->setFlashdata('search', null);
        }
        $data['pager'] = $model->pager;

        echo view('room/view_all', $this->withIon($data));

 * */