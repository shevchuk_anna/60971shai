<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class HotelRoom extends Migration
{
    public function up()
    {
        if (!$this->db->tableexists('Корпус'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'name_house' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE)
            ));
            // create table
            $this->forge->createtable('Корпус', TRUE);
        }

        if (!$this->db->tableexists('Комната'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ID_HOUSING' => array('type' => 'INT','unsigned' => TRUE,'null' => TRUE),
                'NumRoom' => array('type' => 'INT', 'null' => FALSE),
                'NumOfSeats' => array('type' => 'INT', 'null' => FALSE),
                'Cost' => array('type' => 'INT', 'null' => FALSE),
                'user_id' => array('type' => 'int', 'null' => TRUE),
            ));
            $this->forge->addForeignKey('ID_HOUSING','Корпус','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('user_id','users','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('Комната', TRUE);
        }

        if (!$this->db->tableexists('Гость'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'FullName' => array('type' => 'VARCHAR', 'constraint' => '255', 'null' => FALSE)
            ));
            // create table
            $this->forge->createtable('Гость', TRUE);
        }

        if (!$this->db->tableexists('Бронирование'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ID_ROOM' => array('type' =>'INT','unsigned' => TRUE),
                'ID_PEOPLE' => array('type' =>'INT','unsigned' => TRUE,'null' => FALSE),
                'DateStart' => array('type' => 'DATE', 'null' => FALSE),
                'DateEnd' => array('type' => 'DATE', 'null' => FALSE),
                'NumPeople' => array('type' => 'INT', 'null' => FALSE)
            ));
            $this->forge->addForeignKey('ID_ROOM','Комната','id','RESTRICT','RESTRICT');
            $this->forge->addForeignKey('ID_PEOPLE','Гость','id','RESTRICT','RESTRICT');
            // create table
            $this->forge->createtable('Бронирование', TRUE);
        }

        // activity_type
        if (!$this->db->tableexists('Проживание'))
        {
            // Setup Keys
            $this->forge->addkey('id', TRUE);

            $this->forge->addfield(array(
                'id' => array('type' => 'INT', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
                'ID_ROOM' => array('type' =>'INT','unsigned' => TRUE, 'null' => FALSE),
                'ID_PEOPLE' => array('type' =>'INT','unsigned' => TRUE, 'null' => FALSE),
                'DateStart' => array('type' => 'DATE', 'null' => FALSE),
                'DateEnd' => array('type' => 'DATE', 'null' => FALSE),
                'NumPeople' => array('type' => 'INT', 'null' => FALSE)
            ));
            $this->forge->addForeignKey('ID_ROOM','Комната','id','RESTRICT','RESRICT');
            $this->forge->addForeignKey('ID_PEOPLE','Гость','id','RESTRICT','RESRICT');
            // create table
            $this->forge->createtable('Проживание', TRUE);
        }
    }

    //--------------------------------------------------------------------

    public function down()
    {
        $this->forge->droptable('Проживание');
        $this->forge->droptable('Бронирование');
        $this->forge->droptable('Гость');
        $this->forge->droptable('Комната');
        $this->forge->droptable('Корпус');
    }
}
