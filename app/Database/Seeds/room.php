<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class room extends Seeder
{
    public function run()
    {
        //--------------------------КОРПУС-------------------------------
        $data = [
            'name_house' => 'Даниловская мануфактура',
        ];
        $this->db->table('Корпус')->insert($data);

        $data = [
            'name_house' => 'Эталон',
        ];
        $this->db->table('Корпус')->insert($data);

        $data = [
            'name_house' => 'Георгиевский',
        ];
        $this->db->table('Корпус')->insert($data);

        $data = [
            'name_house' => 'Возрождение',
        ];
        $this->db->table('Корпус')->insert($data);



        //--------------------КОМНАТА--------------------------------
        $data = [
            'ID_HOUSING' => '1',
            'NumRoom' => '10',
            'NumOfSeats' => '2',
            'Cost' => '1500',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '1',
            'NumRoom' => '20',
            'NumOfSeats' => '1',
            'Cost' => '1000',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '1',
            'NumRoom' => '30',
            'NumOfSeats' => '3',
            'Cost' => '2000',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '1',
            'NumRoom' => '40',
            'NumOfSeats' => '5',
            'Cost' => '4500',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '2',
            'NumRoom' => '100',
            'NumOfSeats' => '1',
            'Cost' => '7500',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '2',
            'NumRoom' => '200',
            'NumOfSeats' => '2',
            'Cost' => '13350',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '2',
            'NumRoom' => '300',
            'NumOfSeats' => '1',
            'Cost' => '5500',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '2',
            'NumRoom' => '400',
            'NumOfSeats' => '4',
            'Cost' => '15950',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '3',
            'NumRoom' => '1',
            'NumOfSeats' => '1',
            'Cost' => '850',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '3',
            'NumRoom' => '2',
            'NumOfSeats' => '2',
            'Cost' => '900',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '3',
            'NumRoom' => '3',
            'NumOfSeats' => '1',
            'Cost' => '450',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '3',
            'NumRoom' => '4',
            'NumOfSeats' => '4',
            'Cost' => '1150',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '4',
            'NumRoom' => '2',
            'NumOfSeats' => '4',
            'Cost' => '4250',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '4',
            'NumRoom' => '4',
            'NumOfSeats' => '1',
            'Cost' => '2250',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '4',
            'NumRoom' => '8',
            'NumOfSeats' => '2',
            'Cost' => '3250',
        ];
        $this->db->table('Комната')->insert($data);

        $data = [
            'ID_HOUSING' => '4',
            'NumRoom' => '4',
            'NumOfSeats' => '2',
            'Cost' => '3950',
        ];
        $this->db->table('Комната')->insert($data);
        //-------------------ГОСТЬ-------------------------
        $data = [
            'FullName' => 'Шевчук А.И.'
        ];
        $this->db->table('Гость')->insert($data);

        $data = [
            'FullName' => 'Баженова А.С.'
        ];
        $this->db->table('Гость')->insert($data);

        $data = [
            'FullName' => 'Кузнецов А.А.'
        ];
        $this->db->table('Гость')->insert($data);

        $data = [
            'FullName' => 'Потапов В.А.'
        ];
        $this->db->table('Гость')->insert($data);

        $data = [
            'FullName' => 'Корепанов А.А.'
        ];
        $this->db->table('Гость')->insert($data);

        $data = [
            'FullName' => 'Иванов В.В.'
        ];
        $this->db->table('Гость')->insert($data);

        $data = [
            'FullName' => 'Пупкин Ф.К.'
        ];
        $this->db->table('Гость')->insert($data);

        $data = [
            'FullName' => 'Краснопёров С.В.'
        ];
        $this->db->table('Гость')->insert($data);

        $data = [
            'FullName' => 'Алексеев М.Е.'
        ];
        $this->db->table('Гость')->insert($data);

        $data = [
            'FullName' => 'Архарова Т.И.'
        ];
        $this->db->table('Гость')->insert($data);
        //------------------БРОНИРОВАНИЕ--------10 чел-----16 комнат------------
        $data = [
            'ID_ROOM' => '1',
            'ID_PEOPLE' => '1',
            'DateStart' => '2020-12-11',
            'DateEnd' => '2020-12-19',
            'NumPeople' => '2'
        ];
        $this->db->table('Бронирование')->insert($data);

        $data = [
            'ID_ROOM' => '2',
            'ID_PEOPLE' => '2',
            'DateStart' => '2020-11-20',
            'DateEnd' => '2020-11-19',
            'NumPeople' => '1'
        ];
        $this->db->table('Бронирование')->insert($data);

        $data = [
            'ID_ROOM' => '3',
            'ID_PEOPLE' => '3',
            'DateStart' => '2021-01-20',
            'DateEnd' => '2021-01-25',
            'NumPeople' => '3'
        ];
        $this->db->table('Бронирование')->insert($data);

        $data = [
            'ID_ROOM' => '4',
            'ID_PEOPLE' => '4',
            'DateStart' => '2021-02-01',
            'DateEnd' => '2021-02-14',
            'NumPeople' => '2'
        ];
        $this->db->table('Бронирование')->insert($data);

        $data = [
            'ID_ROOM' => '5',
            'ID_PEOPLE' => '5',
            'DateStart' => '2021-03-03',
            'DateEnd' => '2021-03-10',
            'NumPeople' => '1'
        ];
        $this->db->table('Бронирование')->insert($data);

        $data = [
            'ID_ROOM' => '6',
            'ID_PEOPLE' => '6',
            'DateStart' => '2020-12-01',
            'DateEnd' => '2020-12-22',
            'NumPeople' => '2'
        ];
        $this->db->table('Бронирование')->insert($data);
        //---------------ПРОЖИВАНИЕ---------------------------------
        $data = [
            'ID_ROOM' => '2',
            'ID_PEOPLE' => '2',
            'DateStart' => '2020-11-20',
            'DateEnd' => '2020-11-19',
            'NumPeople' => '1'
        ];
        $this->db->table('Проживание')->insert($data);

        $data = [
            'ID_ROOM' => '6',
            'ID_PEOPLE' => '6',
            'DateStart' => '2020-12-01',
            'DateEnd' => '2020-12-22',
            'NumPeople' => '2'
        ];
        $this->db->table('Проживание')->insert($data);

        $data = [
            'ID_ROOM' => '1',
            'ID_PEOPLE' => '1',
            'DateStart' => '2020-12-11',
            'DateEnd' => '2020-12-19',
            'NumPeople' => '2'
        ];
        $this->db->table('Проживание')->insert($data);

        $data = [
            'ID_ROOM' => '3',
            'ID_PEOPLE' => '3',
            'DateStart' => '2021-01-20',
            'DateEnd' => '2021-01-25',
            'NumPeople' => '3'
        ];
        $this->db->table('Проживание')->insert($data);

        $data = [
            'ID_ROOM' => '4',
            'ID_PEOPLE' => '4',
            'DateStart' => '2021-02-01',
            'DateEnd' => '2021-02-14',
            'NumPeople' => '2'
        ];
        $this->db->table('Проживание')->insert($data);
    }
}