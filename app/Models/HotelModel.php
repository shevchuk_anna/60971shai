<?php namespace App\Models;
use CodeIgniter\Model;

class HotelModel extends Model
{
    protected $table = 'Комната'; //таблица, связанная с моделью
    protected $allowedFields = ['ID_HOUSING','NumRoom','NumOfSeats','Cost','user_id','picture_url'];

    public function getRoom($user_id = null, $search = '', $per_page = null)
    {
        $model = $this->like('name', is_null($search) ? '' : $search, 'both');

        if (!is_null($user_id)) {
            $model = $model->where('user_id', $user_id);
        }
        // Пагинация
        return $model->paginate($per_page, 'group1');

       /* if (!isset($id)) {
            return $this->findAll();
        }
        return $this->where(['id' => $id])->first();*/
    }

    // Если $user_id == null то возвращаются все рейтинги, иначе - только принадлежащие пользователю с user_id
    public function getRooms($user_id = null, $search = '', $per_page = null)
    {
        $model = $this->select('*');
        if (!is_null($user_id)) {
            $model = $model->where('user_id', $user_id);
        }
        // Пагинация
        return $model->paginate($per_page, 'group1');
    }
    public function getRoomWithUser($id = null, $search = '')
    {
        $builder = $this->select('Комната.id,Комната.picture_url,NumRoom,NumOfSeats,Cost,email')
            ->join('users','user_id = users.id');
        if($search) {
           $builder->
            where('Cost', (int)$search)
                ->orWhere('NumOfSeats', (int)$search)
                ->orWhere('email',$search,'both',null,true);

            //->like('email',$search,'both',null,true);
        }
        if (!is_null($id))
        {
            return $builder->where(['Комната.id' => $id])->first() ;
        }
        return $builder;
    }
}