<?php

namespace App\Services;

use OAuth2\GrantType\UserCredentials;
use OAuth2\Server;
use OAuth2\Request;
use App\Models\OAuthModel;

class OAuth
{
    public $server;
    private $dsn = 'pgsql:host=ec2-54-74-14-109.eu-west-1.compute.amazonaws.com;port=5432;dbname=d6510ncs72go04;user=onjeykjezrtqkp;password=3d57c37668c21e8cd0794d0f941de41a92b164e16a474e33c041307c5cc7e3d7;';
    function __construct()
    {
        $this->init();
    }

    public function init()
    {
        $storage = new MyPdo(array('dsn' => $this->dsn),array('user_table' => 'users'));
        $grantType = new UserCredentials($storage);
        $this->server = new Server($storage);
        $this->server->addGrantType($grantType);
    }

    public function isLoggedIn()
    {
        return $this->server->verifyResourceRequest(Request::createFromGlobals());
    }

}