<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="jumbotron text-center" style="background: #eef2d4; opacity: 0.80">
        <img class="mb-4" src="https://www.flaticon.com/svg/static/icons/svg/608/608912.svg" alt="" width="72" height="72"><h1 class="display-4">Hotel Information</h1>
        <p class="lead">Это приложение поможет узнать информацию о номерах в отеле</p>
        <?php if (! $ionAuth->loggedIn()): ?>
            <a class="btn btn-primary btn-lg"  style="background-color: #483D8B;border-color: #483D8B" href="auth/login" role="button">Войти</a>
        <?php endif?>
    </div>
<?= $this->endSection() ?>

