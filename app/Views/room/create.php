<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<style>
    .form-group{
        font-size: 20px;
    }
    .eff:hover{
        opacity: 0.8 !important;

    }
</style>
<div class="container" style="max-width: 540px;">
    <?= form_open_multipart('hotel/store'); ?>

    <div class="form-group">
        <label for="NumRoom">Номер комнаты:</label>
        <input id="name" type="text" class="form-control <?= ($validation->hasError('NumRoom')) ? 'is-invalid' : ''; ?>"
               name="NumRoom" value="<?=old('NumRoom')?>">
        <div class="invalid-feedback">
            <?= $validation->getError('NumRoom') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="NumOfSeats">Количество мест:</label>
        <input id="name" type="text" class="form-control <?= ($validation->hasError('NumOfSeats')) ? 'is-invalid' : ''; ?>"
               name="NumOfSeats" value="<?=old('NumOfSeats')?>">
        <div class="invalid-feedback">
            <?= $validation->getError('NumOfSeats') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="Cost">Стоимость:</label>
        <input id="name" type="text" class="form-control <?= ($validation->hasError('Cost')) ? 'is-invalid' : ''; ?>"
               name="Cost" value="<?=old('Cost')?>">
        <div class="invalid-feedback">
            <?= $validation->getError('Cost') ?>
        </div>
    </div>

    <div class="form-group">
        <label for="birthday">Изображение</label>
        <input type="file" class="form-control-file <?= ($validation->hasError('picture')) ? 'is-invalid' : ''; ?>" name="picture">
        <div class="invalid-feedback">
            <?= $validation->getError('picture') ?>
        </div>
    </div>

    <div class="form-group">
        <button type="submit" class="btn btn-primary eff" style="background-color: #483D8B;border-color: #483D8B;
        font-size: 20px; margin-left: 10em" name="submit">Создать</button>
    </div>

    </form>
</div>

<?= $this->endSection() ?>