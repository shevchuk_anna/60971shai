<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main" >
        <?php use CodeIgniter\I18n\Time; ?>
        <?php if (!empty($room)) : ?>
            <div class="card mb-3" style="border: none; border-radius: 0.1;box-shadow: 0 0 7px;">
                <div class="row" >
                    <div class="col-md-4 d-flex align-items-center">
                        <?php if (is_null($room['picture_url'])) : ?>

                        <?php else:?>
                            <img src="<?= esc($room['picture_url']); ?>" class="card-img" alt="<?= esc($room['NumRoom']); ?>">
                        <?php endif ?>

                    </div>
                    <div class="col-md-8"style="font-size: 20px; ">
                        <div class="card-body">
                            <div class="d-flex justify-content-between">
                                <div class="my-0">Номер комнаты:</div>
                                <div class="text-muted">№<?= esc($room['NumRoom']); ?></div>
                            </div>

                            <div class="d-flex justify-content-between">
                                <div class="my-0">Количетсво мест:</div>
                                <div class="text-muted"><?= esc($room['NumOfSeats']); ?> человек</div>
                            </div>

                            <div class="d-flex justify-content-between">
                                <div class="my-0">Стоимость:</div>
                                <span><?= esc($room['Cost']); ?> рублей</span>
                            </div>
                        </div>
                        <?php if ($ionAuth->isAdmin()): ?>
                            <a style="margin-left: 2em;font-size: 17px;background-color: #483D8B;border-color: #483D8B" href="<?= base_url()?>/index.php/hotel/edit/<?= esc($room['id']); ?>" class="btn btn-primary btn-sm">Редактировать</a>
                            <a style="margin-left: 0.5em;font-size: 17px" href="<?= base_url()?>/index.php/hotel/delete/<?= esc($room['id']); ?>" class="btn btn-danger btn-sm">Удалить</a>
                        <?php endif ?>
                         </div>
                </div>
            </div>
        <?php else : ?>
            <p>Рейтинг не найден.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>