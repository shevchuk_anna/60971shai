<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
    <div class="container main" >
        <h2 style="margin-bottom: 1em">Все комнаты</h2 style>
        <?php if (!empty($room) && is_array($room)) : ?>
            <?php foreach ($room as $item): ?>
                <div class="card mb-3" style="max-width: 900px; background-color: #eef2d4; border: none; border-radius: 0.1; opacity: 0.88">
                    <div class="row" >
                        <div class="col-md-4 d-flex align-items-center" style="border-radius: 0;">
                            <?php if (is_null($item['picture_url'])) : ?>

                            <?php else:?>
                                <img src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['NumRoom']); ?>">
                            <?php endif ?>

                        </div>
                        <div class="col-md-8" >
                            <div class="card-body">
                                <h5 class="card-title" style="font-size: 25px">Комната № <?= esc($item['NumRoom']); ?></h5>
                                <p class="card-text" style="font-size: 19px">Количество мест: <?= esc($item['NumOfSeats']); ?></p>
                                <a style="background-color: #483D8B;border-color: #483D8B" href="<?= base_url()?>/index.php/hotel/view/<?= esc($item['id']); ?>"class="btn btn-primary">Просмотреть</a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else : ?>
            <p>Невозможно найти комнату.</p>
        <?php endif ?>
    </div>
<?= $this->endSection() ?>