<?= $this->extend('templates/layout') ?>
<?= $this->section('content') ?>
<div class="container main">
    <div class="d-flex justify-content-between mb-2">
        <?= $pager->links('group1', 'my_page') ?>
        <?= form_open('hotel/viewAllWithUsers', ['style' => 'display: flex']); ?>
        <select name="per_page" class="ml-3" aria-label="per_page" style="margin-right: 0.5em">
            <option value="2" <?php if($per_page == '2') echo("selected"); ?>>2</option>
            <option value="4"  <?php if($per_page == '4') echo("selected"); ?>>4</option>
            <option value="5"  <?php if($per_page == '5') echo("selected"); ?>>5</option>
        </select>
        <button class="btn" type="submit" class="btn btn-primary" style="background-color: #483D8B;border-color: #483D8B; color: #d9edf7; margin-left: 0.2em">На странице</button>
        </form>

        <?= form_open('hotel/viewAllWithUsers',['style' => 'display: flex']); ?>
        <input type="text" class="form-control ml-3" name="search" placeholder="Количество мест" aria-label="Search"
               value="<?= $search; ?>">
        <button class="btn " style="background-color: #483D8B;border-color: #483D8B; color: #d9edf7; margin-left: 0.2em" type="submit" class="btn btn-primary">Найти</button>
        </form>
    </div>
    <?php if (!empty($room) && is_array($room)) : ?>
        <h2>Все комнаты:</h2>
        <table class="table table-striped" style="text-align: center">
            <thead >
                <th scope="col">Фото</th>
                <th scope="col">Номер комнаты</th>
                <th scope="col">Кол-во мест</th>
                <th scope="col">Управление</th>
                <th scope="col">Стоимость(руб.)</th>
                <th scope="col">e-mail</th>
            </thead>
            <tbody>
            <tbody style="text-align: center; font-size:20px">
            <?php foreach ($room as $item): ?>
                <tr>
                    <td>
                        <?php if (is_null($item['picture_url'])) : ?>
                        <?php else:?>
                            <img src="<?= esc($item['picture_url']); ?>" class="card-img" alt="<?= esc($item['NumRoom']); ?>">
                        <?php endif ?>
                    </td>

                    <td style="font-size: 25px"><?= esc($item['NumRoom']); ?></td>
                    <td style="font-size: 25px"><?= esc($item['NumOfSeats']); ?></td>


                    <td>
                        <a style="margin-bottom: 15px; font-size: 16px; background-color: #483D8B;border-color: #483D8B" href="<?= base_url()?>/index.php/hotel/view/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Просмотреть</a><br>
                        <a style="margin-bottom: 70px; font-size: 16px; background-color: #483D8B;border-color: #483D8B" href="<?= base_url()?>/index.php/hotel/edit/<?= esc($item['id']); ?>" class="btn btn-primary btn-sm">Редактировать</a><br>
                        <a  href="<?= base_url()?>/index.php/hotel/delete/<?= esc($item['id']); ?>" class="btn btn-danger btn-sm">УДАЛИТЬ</a>
                    </td>

                    <td style="font-size: 25px"><?= esc($item['Cost']); ?></td>
                    <td ><?= esc($item['email']); ?></td>

                </tr>
            <?php endforeach; ?>
            </tbody>
            </tbody>
        </table>

    <?php else : ?>
        <div class="text-center">
            <p>Комнаты не найдены </p>
            <?= form_open('hotel/index',['style' => 'display: flex']); ?>

            </form>

            <a class="btn btn-primary btn-lg" href="<?= base_url()?>/hotel/store"><span class="fas fa-tachometer-alt" style="color:white"></span>&nbsp;&nbsp;Создать комнату</a>
        </div>
    <?php endif ?>
</div>
<?= $this->endSection() ?>

